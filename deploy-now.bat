echo ON
rmdir /S /Q export
rem call npm rm decentraland-ecs
rem call npm rm -g decentraland
echo ON
rem call npm i -g decentraland@3.2.0
rem call npm i decentraland-ecs@6.1.5
echo ON
copy scene-export.json scene.json /Y
call dcl build
call dcl export
copy now.json export\.
cd export
call now
cd ..
copy scene-deploy.json scene.json /Y
rem call npm rm decentraland-ecs
rem call npm rm -g decentraland
echo ON
rem call npm i -g decentraland
rem call npm i decentraland-ecs@latest
echo ON
