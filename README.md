# Builder HUD for Decentraland
By Carl Fravel

A UI/HUD that a Decentraland scene developer working in the SDK can temporarily add to their scene to manipulate and fine tune the position, rotation and scale of existing or new entities.

Donations/tips greatly appreciated,
  by Ethers or Mana to 0x63F587Fc66d140368bE6F808785937d63b8E6Fc4
  or by PayPal to cfravel@fravel.net

The code repository is at https://gitlab.com/dcl-tech/cf-builder-hud

## The Problem that this HUD solves
Today it is hard to accurately place or adjust the position, rotation or scale of scene entities when working in the SDK.
You must adjust the numeric parameters in script, preview the scene, adust some more, preview the scene again, shampoo, rinse, repeat.
This can be quite laborious.
And if you originally made the scene in the Decentraland Builder, JanusVR, Unity, etc, and brought it into the SDK to add scripts or make other changes, you can't just go back to your original tool with the modified scene.

## How this Builder HUD solves the problem
This HUD can assist a scene developer working in the SDK to set the position, rotation and scale of one or more objects in a scene while working "in world".  The results can then easily be copied as typescript syntax code from the browser Console window back into the scene code.

## Summary of use
In your games.ts code, add this BuilderHUD module by using an import statement, make an instance of the HUD, and optionally attach it to one or more existing entities that you wish to edit, using the attachToEntity() method.

Preview the scene, expand the HUD, and use it to select or add entities, adjust their position, rotation or scale, and save the results to the browser F12 Console.  Copy the console code into your script code, using the parameters for existing objects, or the entire "spawn" statement for new objects you added, and re-load your scene.

The console output uses the syntax of the companion SpawnerFunctions module, so importing it will make adding new entities with the HUD really easy.

## Detailed instructions for use:

* Copy the following from this module into your scene:
  - src/modules/BuilderHUD.ts
  - src/modules/SpawnerFunctions.ts  // to make it easier to use the output of this hud in your code.
  - models/xyz/xyzLeftHand.glb
  - models/pointer/pointer.glb
  - materials/builder-hud/builder-hud-atlas-1024.png

* In your game.ts file, import this module using:
  import {BuilderHUD} from './modules/BuilderHUD'

* If you want to use the spawner function syntax most compatible with the output of this HUD, then add:
  import { spawnEntity, spawnGltfX,spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
  // add or delete particular exported functions as needed to/from the import statement

* In your game.ts code, instantiate the BuilderHUD object

* Optionally, if the Entity(s) that you want to manipulate are already in the scene code, then call the attachToEntity method passing in that Entity.

* You may attach to multiple existing entities that you want to edit by calling attachToEntity for each of them.  Existing entities to which you attach may have parents or not.

* If any new entities you wish to add using the HUD should be parented, pass that parent in using the setDefaultParent() method.

* Preview the scene using dcl start.

* You will see the HUD appear, minimized, when you enter the parcel boundaries of your scene, and it will disappear when you are outside the scene. YOU MUST BE INSIDE THE BOUNDARIES OF YOUR SCENE TO SEE THE HUD APPEAR.

* Go into the scene and maximize the HUD

* Find the object to which you attached the hud.  It will be selected, as indicated by a rotating red pointer above it.  If you attached to several objects, or add new ones, you can change the selection with the Next/Previous buttons the HUD.  Details of the UI are provided below.

* To instantiate a new entithy, go stand and face exactly where you want an object to be located and rotated, and press the New object button (star) on the HUD. A new xyz placeholder widget will appear under your camera, at the level of what you are standing on, beit the ground or some higher location.  the new XYZ placeholder will be oriented in the same y rotation as your camera, and it will be selected for editing in your HUD.

* For each existing or new object, move, size, rotate it until you are satisfied.  You can use the arrow buttons at the top of the HUD to adjust the values.  Use the Editing Mode button to select what parameters you want to edit -- position, rotation, or scale. Use the increment button to set how much an arrow affects the parameters.

* Optionally proceed to use New to instantiate additional copies of the xyz widget in new locations and similarly manipulate and record their coordinates

* Press the Save button to write the 9 parameters for each object out to the Browser's developr Console.  You can open the console using F12, either during the scene loading, or at any time, to see the output from the Builder HUD. In the console ouput you will see lines representing each existing and new entity, in the order a) existing objects atteched in your code in the order you attached to them, and then b) the new objects you added, in the order you added them.  For each such entity you will see 9 numbers, representing position, rotation and scale.  The syntax of the output in the console is designed to be copied and pasted into the the parematers of the SpawnerFunctions for existing objects, and provides the full syntax for new entities (although you will need to tweak it for the entity type.)

* In other words, edit your src/game.ts file and find the existing spawner call(s) of your existing object(s) and paste in the 9 coordinates from the console output.  For each new object you added, add a spawner call to your code, and past in its parameters.  Note that the console output just uses spawn({9 params}), not spawnType().  You will want to edit the function call for the type of entity you are spawning.

* To start over, or to discard your changes, just reload the scene.  The scene is not actually changed permanently by the HUD.  Changes to the scene only become permanent if you copy the generated code back into your scene code.


## UI controls on the HUD
Starting in the lower right, working to the left in each row going up the HUD:

### Minimize/Maximize the HUD

### Save 
Writes the parameters of each existing and new entity as typescript-ready code in the console

### Star: New Entity
Spawns a new entity at the level of the bottom of your feet, exactly under your camera and facing the direction you are facing. This is an XYZ indicator widget that will serve as a placeholder for where you want to place a new entity in your script. the RGB pointers refer to the XYZ coordinate system of the widget.

### Trash Can
Delete the currently selected entity from the list of selectable entities, and, if it is a newly added entity, delete it from the scene.

### Next and Previous
Changes which of the entities is currently selected for editing.  This will go through the entities in the order you attached to them in code and any you added using the HUD.  You will see a red spinning indicator over the selected entity if the HUD is maximized.  The selection indicator disappears if you minimize the HUD.

### Increment and Edting Mode
The Editing Mode indicates Position, Rotation or Scale, and can be clicked to change the mode.
The Increment indicator shows how much change will be applied to postion, rotation or scale, in meters or degrees.

### Six arrow buttons
These buttons edit the position, rotation or scale of the selected entity (depending on the Editing Mode), and by the amount indicated on the Increment button.

# See a demo of this scene
at:
https://demo-builder-hud.cfravel.now.sh

# Tutorial from the Building course, lesson 5 showing how to use the BuilderHUD
https://docs.google.com/document/d/1AF9l3lYzQsiwFcIexGKP3TI-FFUcMVE34Su6U7FuhZo/edit#heading=h.ezbda09ja5d2
and the video of the live session including a demo of the BuilderHUD:
https://www.youtube.com/watch?v=H_27jswjG84


## Notes
The increment button sets the amount of change that will happen.  It is not a snap grid.

## Known Issues
* FIXED 7/21/2020: The first new object added by the HUD will always be at 0,0 and unrotated, no matter where your avatar's camera is.  Ignore it in the output, or move it to where you want it to be.

* Fixed 7/25/2021: new entities are at the level of the bottom of your feet, so they are properly placed even if you are at a higher location.

* Although setDefaultParent works correctly for new entity positions and scale when the new entity has a parent, and the HUD works fine for editing existing objects in a scene where the parent of the object is rotated, the HUD does not yet work correctly for NEW entities if the parent entity is rotated.  If you are working in a scene with a rotated scene parent, and wish to instantiate new entities with that rotated scene entity as their setDefaultParent, it may be best to temporarily set the the rotation of the scene to 0,0,0, add and manipulate the new entity, add it to your scene script, then re-rotate the scene and perhaps do further editing on it.

* Delete Selected Entity button isn't implemented.

## Acknowlegement
I wish to acknowledge JanusVR (https://janusvr.com) for their friendship and expertise.
I was inspired by their use of keystrokes (QWEASD) in their in-world editor as a means to edit the DCL entity Transforms using 6 buttons, in this Builder HUD.
Check them out, they have a great tool set and a great team, a hosting environment at https://vesta.janusvr.com, and the latter, at the UV Pro subscription level, has an excellent tool for exporting scenes from their collaborative VR editing environment to the Decentraland SDK.

