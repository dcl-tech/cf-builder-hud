//////////////////////////////////////////
// SpawnerFunctions
// (c) 2019, 2020 by Carl Fravel
// V2.0 Now adds an EntityName component to entities it creates, with an optional name param added to every spawner function. 
//      Backward compatible with previous version.

// @Component("EntityName")
// export class EntityName {
//   name:string=""
// }

export function spawnEntity(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  // create the entity
  const entity = new Entity(name)
  // set a transform to the entity
  const transform = new Transform({ position: new Vector3(x, y, z) })
  transform.rotation.setEuler(rx, ry, rz)
  transform.scale.set(sx, sy, sz)
  entity.addComponent(transform)
  // let c = new EntityName()
  // c.name = name
  // entity.addComponent(c)
  // add the entity to the engine
  engine.addEntity(entity)
  return entity
}

export function spawnBoxX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new BoxShape())
  return entity
}

export function spawnBox(x: number, y: number, z: number, name: string = "") {
  return spawnBoxX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnConeX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new ConeShape())
  return entity
}

export function spawnCone(x: number, y: number, z: number, name: string = "") {
  return spawnConeX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnCylinderX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new CylinderShape())
  return entity
}

export function spawnCylinder(x: number, y: number, z: number, name: string = "") {
  return spawnCylinderX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnGltfX(s: GLTFShape, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(s)
  return entity
}

export function spawnGltf(s: GLTFShape, x: number, y: number, z: number, name: string = "") {
  return spawnGltfX(s,x,y,z,0,0,0,1,1,1,name)
}

export function spawnPlaneX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new PlaneShape())
  return entity
}

export function spawnPlane(x: number, y: number, z: number, name: string = "") {
  return spawnPlaneX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnSphereX(x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new SphereShape())
  return entity
}

export function spawnSphere(x: number, y: number, z: number, name: string = "") {
  return spawnSphereX(x,y,z,0,0,0,1,1,1,name)
}

export function spawnTextX(value: string, x: number, y: number, z: number, rx: number, ry: number, rz: number, sx: number, sy: number, sz: number, name: string = "") {
  const entity = spawnEntity(x,y,z,rx,ry,rz,sx,sy,sz,name)
  // set a shape to the entity
  entity.addComponent(new TextShape(value))
  return entity
}

export function spawnText(value: string, x: number, y: number, z: number, name: string = "") {
  return spawnTextX(value,x, y,z,0,0,0,1,1,1,name)
}

