/////////////////////////////////////////////////////////
// demo-BuilderHUD
// You are welcome to use any content from this open source scene in compliance with the license.
/*
Copyright 2019 Carl Fravel

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

/////////////////////////////////////////////////////////

//import {spawnGltfX, spawnEntity, spawnBoxX, spawnPlaneX} from './modules/SpawnerFunctions'
import {BuilderHUD} from './modules/BuilderHUD'

//////////////////////////////////////////
// Entities

// This scene entity wraps the whole scene, so it can be moved, rotated, or sized as may be needed
const scene = new Entity()
//@CF TODO i set pos != 0,0,0 to test parent effect
const scenetransform = new Transform({ position: new Vector3(0, 0, 0), rotation: Quaternion.Euler(0, 0, 0), scale: new Vector3(1, 1, 1) })
scene.addComponent(scenetransform)
engine.addEntity(scene)

let grass = new Entity()
let grassShape = new  BoxShape()
grass.addComponent(grassShape)
let grassTexture = new Texture('materials/Tileable-Textures/grassy-512-1-0.png')
let grassMaterial = new Material()
grassMaterial.albedoTexture = grassTexture
grass.addComponent(grassMaterial)
let grassTransform = new Transform({position: new Vector3(8,0,8), scale: new Vector3(16,0.01,16)})
grass.addComponent(grassTransform)
grass.setParent(scene)


////////// Example of existing Entities to manipulate with the BuilderHUD
const existingEntity = new Entity()
const transformE = new Transform({ position: new Vector3(2, 1, 5), scale: new Vector3(0.3, 0.3, 0.3) })
existingEntity.addComponent(transformE)
existingEntity.addComponent(new BoxShape())
engine.addEntity(existingEntity)
existingEntity.setParent(scene)

const existingEntity2 = new Entity()
const transformE2 = new Transform({ position: new Vector3(8, 1, 5), scale: new Vector3(0.3, 0.3, 0.3) })
existingEntity2.addComponent(transformE2)
existingEntity2.addComponent(new BoxShape())
engine.addEntity(existingEntity2)
existingEntity2.setParent(scene)

///////// Example of adding the BuilderHUD to your scene and attaching it to one existing scene entity.
const hud:BuilderHUD =  new BuilderHUD()
hud.attachToEntity(existingEntity)
hud.attachToEntity(existingEntity2)
hud.setDefaultParent(scene)

